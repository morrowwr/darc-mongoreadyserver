
/**
 * @module logger
 * @description This module wraps `log4js` to enable nice logging. The actual result of
 *      `require('./logger.js')` is a function that creates a logger object from `_config` 
 *      and returns wrappers (as object fields) to the logging functions. 
 * 
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * 
 * IMPORTS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// actual logging package
const _log4js = require( 'log4js' );

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * 
 * INTERNAL
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const logs = ['all','trace','debug','info','warn','error','fatal','mark','off'];

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * MODULE LOAD FUNCTION
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** 
 * @function 
 * @description Load function for setting up logger from `log4js`. Returned from `require(`./logger.js`)`. 
 * 
 * @param {object} c `_config` object with configuration details
 * 
 * @returns {object} Object with fields wrapping the `logger` logging functions (`all`, `trace`, `debug`, 
 *      `info`, `warn`, `error`, `fatal`, `mark`, `off`)
 * 
 */
const load = ( c ) => { 

  var userSpecifiedLogs = c.logs ? c.logs : {};

  var logappenders  = { out : { type: 'stdout' } }
  var logcategories = { default : { appenders : [ 'out' ] , level : 'info' } }

  // ALL < TRACE < DEBUG < INFO < WARN < ERROR < FATAL < MARK < OFF

  logs.forEach( L => {
    if( userSpecifiedLogs[L] ) {
      userSpecifiedLogs[L].forEach( usl => {
        logappenders[ usl.name ] = ( usl.file ? { type: 'file' , filename: usl.file } : { type: 'stdout' } );
        if( logcategories[L] ) { logcategories[L].appenders.push( usl.name ); }
        else { logcategories[L] = { appenders : [ usl.name ] , level : L }; }
      } );
    }
  } );

  _log4js.configure( { appenders : logappenders , categories : logcategories } );

  // define function wrappers that write to this log4js setup
  var log4jsLogs = {};
  logs.forEach( L => { log4jsLogs[ L ] = ( a ) => ( _log4js.getLogger(L)[L]( a ) ); } );

  // return the logs from the load function
  return log4jsLogs;

};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * MODULE EXPORTS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// export a function that can be called to generate logger
module.exports = load;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * 
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */