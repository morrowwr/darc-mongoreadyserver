
const _crypto = require( 'crypto' );

var _log = undefined;

// return a value, but "protected" with a default that can also be passed
const protect = ( a , b ) => ( a ? a : b );

// uniform timing wrappers
const utc = () => ( ( new Date( Date.now() ) ).toISOString() );
const now = () => ( Date.now() / 1000.0 );

// valid dictionary key
const vdk = ( key , dict ) => ( ( key in dict ) && dict[key] );

// get a random hash
const getHash = () => {
    var current_date = (new Date()).valueOf().toString();
    var random = Math.random().toString();
    return String( _crypto.createHash('sha1').update( current_date + random ).digest('hex') );
}

const getKey = () => {
	return _crypto.randomBytes(12).toString('hex');
}

// 
const eventTag = ( event , uid ) => {
	if( uid ) { return { event : event , user : uid , time  : utc() }; }
	return { event : event , time : utc() };
}

// a uniform treatment of server errors that may occur
const errorResponse = ( res , err ) => {
	_log.error( err );
	_log.info( err.toString() );
	res.status(500).send( err.toString() );
}	

const isString = ( x ) => ( x === x.toString() );
const mdboidRegEx = /[0-9abcdef]{24}/
const _mdboid = require( 'mongoose' ).ObjectId;
const mdboid = ( id ) => ( isString( id ) 
							? ( mdboidRegEx.test( id )
								? _mdboid( id ) 
								: undefined )
							: id );

// exports
module.exports = ( _l ) => { 

	_log = _l;

	return {
		protect  : protect , 
		utc      : utc , 
		now      : now , 
		vdk      : vdk , 
		getHash  : getHash , 
		getKey   : getKey , 
		isString : isString , 
		mdboid   : mdboid , 
		eventTag : eventTag , 
		errorResponse : errorResponse , 
	};

};