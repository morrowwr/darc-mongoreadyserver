
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * OFF-CYCLE CHECK LOOP
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// coordinator, for check loop(s)
const Coordinator = require( 'daat-coordinator' );

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

var _config = undefined , 
	_log 	= undefined ;

var checkLoopTimer = undefined , 
	checkLoopInterval = 24 * 60 * 60 * 1000 , // milliseconds in one day
	offsetToCheckTime = undefined , // defined based on runtime and config
	checkOffsetTimeDetails = { hours : 1 , minutes : 0 , seconds : 0 } , // default to running at 1 AM (UTC)
	runningChecks = false ;

const iso8601dur = /P(([0-9]+Y){0,1}([0-9]+M){0,1}([0-9]+D){0,1}){0,1}(T([0-9]+H){0,1}([0-9]+M){0,1}([0-9]+S){0,1}){0,1}/;

// check loop coordinator
const _coord = new Coordinator();

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * coordinator things... run success and failure functions, and a run wrapper
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// success method for a check loop run by a coordinator
const checkLoopSuccess = () => {
	_log.info( "check loop succeded" );
	runningChecks = false;
};

// failure method for a check loop run by a coordinator
const checkLoopFailure = ( error ) => {
	_log.error( error );
	_log.info( "check loop FAILED: " + error.toString() );
	runningChecks = false;
};

// the main function to run when check loop fires
const runChecks = () => {
	if( ! runningChecks ) {
		runningChecks = true;
		_log.info( "running checks: " + _coord.getStages() );
		_coord.run( checkLoopFailure , checkLoopSuccess ); 
	}
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * OTHER THINGS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// when starting, use an irregular delay defined by the time until the  
const getCheckOffsetTime = (  ) => {

	if( ! checkOffsetTimeDetails ) { return 0; }

	let now = new Date( Date.now() );

	let then = new Date( Date.UTC( 
		now.getFullYear() , 
		now.getMonth() , 
		now.getDate() , 
		checkOffsetTimeDetails.hours , 
		checkOffsetTimeDetails.minutes , 
		checkOffsetTimeDetails.seconds , 
		0 
	) );

	offsetToCheckTime = then - now;
	if( offsetToCheckTime < 0 ) {
		// if we've passed the checkTime today, do tomorrow
		then.setDate( then.getDate() + 1 ); 
		offsetToCheckTime = then - now;
	}

	return offsetToCheckTime;

}

// setup a timeout to the offset, then fire check loop and setup the interval
const startCheckLoop = ( wait ) => {

	offsetToCheckTime = getCheckOffsetTime(); // get offset time from now()

	if( offsetToCheckTime <= 0 ) {
		checkLoopTimer = setInterval( runChecks , checkLoopInterval ); 
		// run NOW because interval won't fire immediately
		if( ! wait ) { runChecks(); }
	} else {
		_log.info( `Offset to regular check time: ${offsetToCheckTime} ms (${(offsetToCheckTime/1000.0/60.0).toFixed(1)} min, ${(offsetToCheckTime/1000.0/60.0/60.0).toFixed(1)} hr)` );
		checkLoopTimer = setTimeout( ( ) => {
			clearTimeout( checkLoopTimer );
			// run checks every checkLoopInterval seconds
			checkLoopTimer = setInterval( runChecks , checkLoopInterval ); 
			// run NOW because interval won't fire immediately
			if( ! wait ) { runChecks(); }
		} , offsetToCheckTime );
	}

};

// clear out the timer, effectively canceling the checkLoop
const stopCheckLoop = () => {
	clearTimeout( checkLoopTimer );
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * MODULE EXPORTS
 * 
 * loading the module will NOT start the checkloop. You have to issue a call to "runChecks" for 
 * the loop to start. 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = ( c , l ) => {

	_config = c , _log = l;

	// how long until FIRST repeated check evaluation? we want the (daily) checks to be done at, sa, 1 AM PT. 
	// so before we fire off those checks we need to get to 1 AM. these get methods return local times. 

	// parse config-defined check loop run date time (ISO 8601 style string)
	if( _config.runCheckLoopAt ) {

		if( /now/.test( _config.runCheckLoopAt ) ) {
			checkOffsetTimeDetails = null;
		} else if( /[0-9]{2}:[0-9]{2}:[0-9]{2}/.test( _config.runCheckLoopAt ) ) {
			let timeDetails = /([0-9]{2}):([0-9]{2}):([0-9]{2})/.exec( _config.runCheckLoopAt );
			checkOffsetTimeDetails = { 
				hours   : Math.max( 0 , Math.min( 23 , parseInt( timeDetails[1] ) ) ) , 
				minutes : Math.max( 0 , Math.min( 59 , parseInt( timeDetails[2] ) ) ) , 
				seconds : Math.max( 0 , Math.min( 59 , parseInt( timeDetails[3] ) ) ) , 
			}
		} else {
			_log.info( `WARNING: runCheckLoopAt "${_config.runCheckLoopAt}" is not a valid ISO 8601 time string.` );
		}

	}

	// get duration (period) for checks, via ISO 8601 string, defaulting to one day
	if( _config.checkLoopInterval ) {
		if( ! iso8601dur.test( _config.checkLoopInterval ) ) {
			_log.info( `WARNING: checkLoopInterval "${_config.checkLoopInterval}" not a valid ISO 8601 duration string.` );
			_config.checkLoopInterval = "P1D";
		}
	} else { _config.checkLoopInterval = "P1D"; }

	// parse out details of datetime using a regex
	const checkLoopIntervalDetails = iso8601dur.exec( _config.checkLoopInterval );

	if( checkLoopIntervalDetails[0].length < _config.checkLoopInterval.length ) {

		_log.info( `WARNING: checkLoopInterval "${_config.checkLoopInterval}" is not a valid ISO 8601 duration string.` );

	} else {

		// [ 0          , 1             , 2     , 3      , 4    , 5            , 6     , 7       , 8       ]
		// [ full match , P time string , years , months , days , "T" if match , hours , minutes , seconds ]
		// ignore years
		// ignore months
		// ignore days too? not doing so right now

		checkLoopInterval = 0;

		if( checkLoopIntervalDetails[8] ) { // seconds
			checkLoopInterval += parseInt( checkLoopIntervalDetails[8] );
		}
		if( checkLoopIntervalDetails[7] ) { // minutes
			checkLoopInterval += parseInt( checkLoopIntervalDetails[7] ) * 60;
		}
		if( checkLoopIntervalDetails[6] ) { // hours
			checkLoopInterval += parseInt( checkLoopIntervalDetails[6] ) * 60 * 60;
		}
		if( checkLoopIntervalDetails[4] ) { // days
			checkLoopInterval += parseInt( checkLoopIntervalDetails[4] ) * 60 * 60 * 24;
		}

		checkLoopInterval *= 1000; // convert to ms for js timers
		
		_log.info( "check loop interval: " + checkLoopInterval );


	}

	// routines to run at daily check time; add (key,function) pairs as you go along

	return {
		stopCheckLoop  : stopCheckLoop , 
		startCheckLoop : startCheckLoop , 
		runChecks      : runChecks , 
		quiet          : _coord.quiet.bind( _coord ) , // it is important to bind
		addStage       : _coord.addStage.bind( _coord ) , // it is important to bind
	};

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Copyright 2019+, Stanford GSB CIRCLE RSS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */