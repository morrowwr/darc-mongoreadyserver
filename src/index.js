/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * MONGODB READY SERVER
 * 
 * Copyright Stanford GSB DARC Team 
 * 
 * Created by W. Ross Morrow, Research Computing Specialist
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

'use strict';

var _log , _util , _ctx;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * MODULE EXPORTS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const loadServer = ( ctx , modules ) => {

	return new Promise( ( resolve , reject ) => {

		// returns a Promise that resolves with a config updated with options and a MongoDB 
		// collections references object for the specified MongoDB collection names. 
		_ctx.mdb.reloadMongoDB( )
			.then( result => { 

				ctx.cfg = result.config; // reloadMongoDB may modify _config with options
				ctx.col = result.col; // collections object

				// server.js will need to know where to find "modules". That is, 
				// files with routines, routes and their handlers, and any checks. 
				// if we want a "forkable" repository, we can just standardize that to 
				// be in src/modules. But if we want an "importable" module, we can't. 
				// we have to pass an argument that defines where the modules are
				// in the project importing the server code. 
				require( './server.js' )( ctx , modules )
					.then( s => { 
						ctx.srv = s; // add the server returned from the promise's resolve to context
						resolve( ctx ); 
					} )
					.catch( reject );

			} ).catch( reject );

	} );

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * MODULE EXPORTS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 
module.exports = ( modules , optargs , checks ) => {

	// is there better then/catch syntax than this explicit Promise wrapping?
	return new Promise( ( resolve , reject ) => {

		require( 'darc-mongoreadyserverconfig' )( optargs , checks )
			.then( c => { 

				_log  = require( './logger.js' )( c ); // this sets up the logs
				_util = require( './utilities.js' )( _log ); // utilities used frequently, across submodules

				// HERE we can setup a server with the defined configuration and collections
				_ctx = {
					dir : /(.*)\/[^\/]*$/.exec( process.mainModule.filename )[1] , 
					pkg : require( `${process.cwd()}/package.json` ) , // load the importing package's "manifest" , 
					cfg : c , // reloadMongoDB may modify _config with options, so we overwrite later
					utl : _util , // utilities used frequently, across submodules 
					log : _log , 
					mdb : require( './mongodb.js' )( c , _log , _util ) , 
					col : undefined , // defined later
					srv : undefined , // no server yet, added after construction below
					mod : {} , // empty placeholder for the modules
				};

				// opening log line in each recording log
				Object.keys( _log ).forEach( l => {
					_log[l]( `* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ` );
					_log[l]( `* ${_ctx.pkg.name} (v ${_ctx.pkg.version}) starting...` );
					_log[l]( `* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ` );
				} );

				// actually load the server
				loadServer( _ctx , modules ).then( resolve ).catch( reject );

			} ).catch( reject );

	} );

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Copyright 2020+, Stanford GSB DARC Team
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */