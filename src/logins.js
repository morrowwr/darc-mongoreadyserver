
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const _axios = require( 'axios' );

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

var _config , _log , _util , loginServer ;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// get authorization from headers, if it exists
const authorization = ( req ) => (
	"headers" in req 
		? ( ( "Authorization" in req.headers 
				? req.headers.Authorization 
				: ( "authorization" in req.headers 
					? req.headers.authorization
					: "unknown" ) ) )
        : "unknown" 
);

// get identity from headers, if it exists
const identity = ( req ) => (
	"headers" in req 
		? ( ( "Identity" in req.headers 
				? req.headers.Identity 
				: ( "identity" in req.headers 
					? req.headers.identity
					: "unknown" ) ) )
        : "unknown" 
);

// check a token against login server defined in config
// 
// It might be possible/preferable to record token validity and expiration time locally, 
// and thus avoid repeated network requests for authorization. If rights can't be 
// "revoked" within the authorization time window, there's not really a downside to 
// this. 
const checkToken = ( token ) => {
	return new Promise( ( resolve , reject ) => {
		loginServer.get( "/" + token )
			.then( response => { resolve( response.data.uid ); } )
			.catch( error => {
				if( error.response ) {
					reject( { status : error.response.status , data : error.response.data } );
				} else {
					reject( { status : 0 , data : "unknown error" } );
				}
			} );
	} );
}

// evaluate whether a request is authorized or not
const isAuthorized = ( req , res , next ) => {

	req.authorization = authorization( req );
	req.identity = identity( req );
	req.authorized = false;

	// checking authorization
	checkToken( req.authorization )
		.then( ( ) => { 
			req.authorized = true;
			next(); // the NEXT middleware could execute RBAC
		} ).catch( ( err ) => { 
			_log.error( err );
			_log.info( "Authorization check error: " + err.toString() ); 
			next();
		} );

};

// fake an evaluation of whether a request is authorized or not
const fakeIsAuthorized = ( req , res , next ) => {
	req.authorization = authorization( req );
	req.identity = identity( req );
	req.authorized = true;
	next();
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = ( ctx ) => {

	// basics
	_config = ctx.cfg;
	_log 	= ctx.log; 
	_util 	= ctx.utl; 

	let enforcingAuthorization = false;

	// create the login server URL
	if( _config.login ) {

		var loginServerURL = ( _config.login.protocol ? _config.login.protocol : "https" ) + "://"
								+ _config.login.host
								+ ( _config.login.port ? ":" + _config.login.port : "" )
								+ "/" + ( _config.login.path ? _config.login.path : "" );

		// create a wrapper object... the idea is that this is called for EVERY request, 
		// if auth is on, and thus slowness here is a bigger problem. 
		loginServer = _axios.create( {
			baseURL : loginServerURL , 
			timeout : 1000 , 
		} );

		enforcingAuthorization = /([Yy](es)*|[Tt](rue)*)/.test( _config.enforce_authorization );

	}

	// return calls the "require"ing code can use
	return { 
		isAuthorized : ( enforcingAuthorization ? isAuthorized : fakeIsAuthorized ) , 
	};

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * Copyright 2019+, Stanford GSB CIRCLE RSS
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */