
let _config;

require( './../src/index.js' )()
	.then( () => {} )
	.catch( err => { console.log( "FAILURE: " , err ); } )
	.finally( process.exit ); // can't use with more than one unit test

// keeps process from exiting
process.stdin.resume();